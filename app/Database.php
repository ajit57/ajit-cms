<?php
namespace AJIT;

use PDO;
use PDOException;
use Exception;
class Database{
    private $database_name;
    private $database_user;
    private $database_pass;
    private $database_host;
    private $db;

    function __construct($array)
    {
        if(is_array($array)){
            if(array_key_exists('database_name', $array)){
                $this->database_name = $array['database_name'];
            }
            if(array_key_exists('database_user', $array)){
                $this->database_user = $array['database_user'];
            }
            if(array_key_exists('database_pass', $array)){
                $this->database_pass = $array['database_pass'];
            }
            if(array_key_exists('database_host', $array)){
                $this->database_host = $array['database_host'];
            }
            try{
                $dsn = 'mysql:host=' .$this->database_host. ';dbname=' .$this->database_name.';charset=utf8';
                $this->db = new PDO($dsn, $this->database_user, $this->database_pass);
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }catch (PDOException $e){
                echo $e->getMessage();
            }
        }else{
            throw new Exception('Configuration not provided....');
        }
}
    public function create($table, $data)
    {
        $keys = array_keys($data);
        foreach($keys as $key){
            $values[] = ':' .$key;
        }
        $query = 'INSERT INTO ' .$table. '('.implode(',', $keys).') VALUES ('.implode(',', $values).')';
        $stm = $this->db->prepare($query);
        foreach($data as $key => $value){
            $stm->bindValue($key, $value);
        }
        $stm->execute();
        return $this->db->lastInsertId();
    }
    public function read($table, $read){
        if(is_array($read)){
            $columns = implode(',', $read);
        } elseif($read == '*'){
            $columns = '*';
        }else{
            $columns = $read;
        }
        $query = 'SELECT '. $columns.' FROM '. $table;
        $stm = $this->db->query($query);
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

}